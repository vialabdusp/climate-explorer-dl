# Climate Explorer DL

## Getting started

### Create Virtual Environment

First, create a virtual environment. If you don't have venv installed, install
it globally using:

```
sudo python3 -m pip install virtualenv
```

Then, create a new virtual environment---I like placing it in the root folder
of the repository---and activate it.

```
# Create the environment...
python3 -m virtualenv ./venv
# ...and activate it.
. ./venv/bin/activate
```
Finally, install dependencies!

```
python3 -m pip install -r requirements.txt
```

You can then run the script like so:

```
python3 ./climate_downloader.py
```
