#!/usr/bin/env python

"""climate_downloader.py: 

Downloads extensive subset of data presented on the Climate Explorer
tool by NOAA, NASA, USGS, EPA, BoR, NEMAC, etc..

https://crt-climate-explorer.nemac.org/

Constructed with the help of the ACIS Query builder.
https://builder.rcc-acis.org/"""

__author__ = "Eric Robsky Huntley"
__copyright__ = "Copyright 2022"
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "Eric Robsky Huntley"
__email__ = "ehuntley@mit.edu"

import requests
import pandas as pd
from census import Census
from time import sleep
import copy

# Set start and end dates.
# ==============
SDATE = 1960
EDATE = 2050

# Use Census package to generate list of all county FIPS codes.
# ==============
# Set API key
C = Census("YOUR CENSUS API KEY HERE.")
# 02 > Alaska
# 15 > Hawai'i
# 72 > Puerto Rico
fips = ['02', '15', '72']
CENSUS = C.acs5.state_county(
    fields = ('NAME'),
    state_fips = "*",
    county_fips = "*",
    year = 2020
)
CENSUS = pd.DataFrame(CENSUS).query(f'state!={fips}')
CENSUS.loc[:,"fips"] = CENSUS["state"] + CENSUS["county"]

# Set up our basic list of queries.
# =================================
# Constructed using the ACIS query builder.
# https://builder.rcc-acis.org/

BASE_URL = "http://grid2.rcc-acis.org/GridData"

BASE_DICT = {
    "county": "",
    "sdate": str(SDATE),
    "edate": str(EDATE),
    # Scenario left off so we can iterate in 
    # query_sequence function.
    "grid": "loca:wmean:",
    # Elems left blank---replaced with queries
    # below.
    "elems":[],
    "output": "json"
}

QUERIES = {
    # Annual days with maximum temperature > 90/80F
    "days_max_temp_g": {
        "name": "maxt",
        "interval": [1],
        "duration":1,
        "reduce":"cnt_gt_",
        "values": [80, 90],
        "area_reduce": "county_mean"
    },
    # Annual days with minimum temperature > 90/80F
    "days_min_temp_g": {
        "name": "mint",
        "interval": [1],
        "duration":1,
        "reduce":"cnt_gt_",
        "values": [80, 90],
        "area_reduce": "county_mean"
    },
    # Total precipitation
    "tot_pcpn": {
        "name": "pcpn",
        "interval": [1],
        "duration": 1,
        "reduce": "sum",
        "area_reduce": "county_mean"
    },
    # Annual days with precipitation < 0.01".
    "days_dry": {
        "name": "pcpn",
        "interval": [1],
        "duration": 1,
        "reduce": "cnt_lt_",
        "area_reduce": "county_mean",
        "values": [0.01]
    },
    # Annual days with precipitation > 1/2/3"
    "days_pcpn_g": {
        "name": "pcpn",
        "interval": [1],
        "duration": 1,
        "reduce": "cnt_gt_",
        "area_reduce": "county_mean",
        "values": [1, 2, 3]
    },
    # Number of annual cooling degree days.
    "days_cooling": {
        "name": "cdd",
        "interval": [1],
        "duration": 1,
        "reduce": "sum",
        "area_reduce": "county_mean"
    },
    # Number of annual heating degree days.
    "days_heating": {
        "name": "hdd",
        "interval": [1],
        "duration": 1,
        "reduce": "sum",
        "area_reduce": "county_mean"
    },
    # Number of annual growing degree days.
    "days_growing": {
        "name": "gdd",
        "interval": [1],
        "duration": 1,
        "reduce": "sum",
        "area_reduce": "county_mean"
    }
}

def get_results(d:dict, name:str, base_url:dict = BASE_URL) -> pd.DataFrame():
    # Read scenario from grid specification.
    scenario = d["grid"].split(':')[2]
    # Request url.
    with requests.post(base_url, json = d) as response:
        # If successful...
        if response.ok: 
            # Parse results to dataframe.
            results = pd.concat([
                pd.DataFrame.from_dict(
                    line[1], 
                    orient='index', 
                    columns=['value']
                ).assign(
                    year = line[0]
                ).assign(
                    var = name
                ).assign(
                    scenario = scenario
                ).rename_axis(
                    "county"
                ).reset_index() for line in response.json()["data"]
            ])
            return results
        # Indicate there is a problem with the request.
        else:
            print(requests.RequestException(f"Warning: Request response did not return ok status code on query including counties {d['county']}. Response: {response.text}"))
            pass

def query_sequence(queries:dict = QUERIES, base_dict:dict = BASE_DICT, geo:pd.DataFrame = CENSUS) -> None:
    # Initialize empty data frame to store results.
    df = pd.DataFrame(columns = ['county', 'value', 'year', 'var', 'scenario'])
    # Segment by state to get more regular updates on progress.
    for state_fips in pd.unique(geo["state"]).tolist():
        print(f"State FIPS code: {state_fips}.")
        for k,v in queries.items():
            # Iterate over two climate scenarios.
            # rp85 is high emission.
            # rp45 is low emission.
            for scenario in ["rcp45", "rcp85"]:
                    # If similar query is run for multiple values
                    # (For example multiple precipitation levels,
                    # threshold temperatures, etc.)
                    if "values" in v.keys():
                        # Loop through parameter values.
                        for value in v["values"]:
                            # Deep copy to ensure that values aren't
                            # changed on original (Python creates 
                            # references on assignment)
                            query = copy.deepcopy(base_dict)
                            # Set counties based on current state.
                            query["county"] = ",".join(geo.query(f"state=='{state_fips}'").loc[:,"fips"])
                            # Indicate high/low emissions scenario.
                            query["grid"] = query["grid"] + scenario
                            # Set elements (i.e., what variable you want returned.
                            # (Expects dictionary in list. ::Shrug::)
                            query["elems"] = [{k: v[k] for k in set(list(v.keys())) - set(["values"])}]
                            # This sets how short-term measurements are reduced
                            # E.g., number of days in an interval over a certain temperature.
                            query["elems"][0]["reduce"] = query["elems"][0]["reduce"] + str(value)
                            print(f"Downloading {k + str(value)} in the {scenario} scenario.")
                            # Call requesting function.
                            results = get_results(query, name = k + str(value))
                            # Add results to dataframe.
                            df = pd.concat([df, results[results['county'].str.contains(f"^{state_fips}")]])
                            # Be courteous!
                            sleep(1)
                    # If query is not run on multiple values...
                    else:
                        # Deep copy to ensure that values aren't
                        # changed on original (Python creates 
                        # references on assignment)
                        query = copy.deepcopy(BASE_DICT)
                        # Set counties based on current state.
                        query["county"] = ",".join(geo.query(f"state=='{state_fips}'").loc[:,"fips"])
                        # Indicate high/low emissions scenario.
                        query["grid"] = query["grid"] + scenario
                        # Set elements (i.e., what variable you want returned.
                        # (Expects dictionary in list. ::Shrug::)
                        query["elems"] = [v]
                        print(f"Downloading {k + str(value)} in the {scenario} scenario.")
                        # Call requesting function.
                        results = get_results(query, name = k)
                        # Add results to dataframe.
                        df = pd.concat([df, results[results['county'].str.contains(f"^{state_fips}")]])
                        # Be courteous!
                        sleep(1)
    # Write to CSV.
    df.drop_duplicates().to_csv("results.csv", index = False)
    return None

if __name__ == "__main__":
    query_sequence()
